<?php

namespace ThetaLabs\OAuth;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class OauthServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // check config
        if ($base = config('oauth2.base_endpoint')) {
            config([
                'oauth2.authorization_endpoint' => Str::finish($base, '/') . 'authorize',
                'oauth2.token_endpoint' => Str::finish($base, '/') . 'token',
            ]);
        }

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/oauth2.php' => config_path('oauth2.php'),
            ], 'oauth-config');
        }
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        if (! $this->app->configurationIsCached()) {
            $this->mergeConfigFrom(__DIR__.'/../config/oauth2.php', 'oauth2');
        }
    }
}
