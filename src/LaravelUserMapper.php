<?php

namespace ThetaLabs\Oauth;

class LaravelUserMapper
{
    public static function map($response)
    {
        if (empty($response['email'])) {
            return NULL;
        }

        $user = \App\Models\User::updateOrCreate(['email' => $response['email']], [
            'name' => $response['name'],
        ]);

        return $user;
    }
}