<?php

namespace ThetaLabs\Oauth\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Illuminate\Routing\Controller;

class OauthController extends Controller
{
    public function authenticate()
    {
        if (auth()->check())
            return redirect()->intended();

        $base = config('oauth2.authorization_endpoint');
        $params = [
            'client_id' => config('oauth2.client_id'),
            'redirect_uri' => route(config('oauth2.redirect_route')),
            'response_type' => config('oauth2.response_type'),
            'state' => csrf_token(),
            'scope' => implode(' ', config('oauth2.scopes')),
        ];

        $url = Str::finish(url($base), '?') . Arr::query($params);

        return redirect()->away($url);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Http\Client\RequestException
     * @throws \Throwable
     */
    public function callback(Request $request)
    {
        // ensure state is good, i.e we started the process
        abort_unless($request->get('state') === csrf_token(), 400,
            'Invalid oauth state, please logging in again'
        );

        // trade the authorization code for a token
        $payload = [
            'grant_type' => 'authorization_code',
            'client_id' => config('oauth2.client_id'),
            'client_secret' => config('oauth2.client_secret'),
            'redirect_uri' => route(config('oauth2.redirect_route')),
            'code' => $request->code,
        ];

        $response = $this->http()->post(config('oauth2.token_endpoint'), $payload);

        if (! $response->ok()) {
            abort(400, 'OAuth error occurred while verifying authentication code. ' . data_get($response, 'hint'));
        }

        $method = config('oauth2.userinfo.method');
        $endpoint = config('oauth2.userinfo.endpoint');

        $response = $this->http()->withToken($response['access_token'])
            ->send($method, $endpoint);

        if (! $response->ok()) {
            abort(400, 'OAuth error occurred while retrieving user. Got error code ' . $response->status() . ' for user information endpoint.');
        }

        $mapper = config('oauth2.user-mapper');
        $user = $mapper::map($response);

        if (! $user) {
            abort(400, 'User mapper did not return a user object.');
        }

        auth()->login($user);

        return redirect()->intended();
    }

    protected function http()
    {
        return app()->isLocal() ? Http::withoutVerifying() : Http::newPendingRequest();
    }

    public function timeout()
    {
        return view('errors.error', [
            'title' => 'Login Expired',
            'code' => 'Session Timeout',
            'message' => 'Your session has timed out due to inactivity.',
            'button' => 'Login'
        ]);
    }

    public function logout()
    {
        auth()->logout();

        if ($url = config('oauth2.logout_redirect')) {
            return redirect()->away(
                str_replace('{:url}', route('logout.complete'), $url)
            );
        }

        return redirect()->route('logout.complete');
    }

    public function loggedOut()
    {
        return view('errors.error', [
            'title' => 'Logged Out',
            'code' => 'Logged Out',
            'message' => 'You have successfully logged out.',
            'button' => 'Login'
        ]);
    }
}
