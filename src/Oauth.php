<?php

namespace ThetaLabs\Oauth;

use Illuminate\Support\Facades\Route;

class Oauth {
    /**
     * Binds the Oauth routes into the controller.
     *
     * @param  array  $options
     * @return void
     */
    public static function routes(array $options = [])
    {
        $defaultOptions = [
            'namespace' => '\ThetaLabs\Oauth\Http\Controllers',
            'middleware' => ['web'],
        ];

        $options = array_merge($defaultOptions, $options);

        Route::group($options, function () {
            Route::get('/login', 'OauthController@authenticate')->name('login');
            Route::get('/logout', 'OauthController@logout')->name('logout');
            Route::get('/timeout', 'OauthController@timeout')->name('login.timeout');
            Route::get('/logged-out', 'OauthController@loggedOut')->name('logout.complete');
            Route::get('/oauth/callback', 'OauthController@callback')->name('oauth2.callback');
        });
    }
}
